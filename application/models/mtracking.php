<?php
class Mtracking extends CI_Model{
	//add new tracking banner
	private $ttracking = 'TTRACKING';
	
	function Tracking(){
		parent::Model();
	}
	
	function gettracking(){
		$q = $this->db->query("	SELECT 
									TTRACKING.TRACKINGFILE AS FILE,
									count(TTRACKING.TRACKINGID) AS CLICK 
								FROM TTRACKING
								GROUP BY FILE
								ORDER BY FILE ASC");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}

	function save_tracking($tracking){
		$this->db->insert($this->ttracking, $tracking);
		return $this->db->insert_id();
	}
}

/*
	CREATE TABLE TUSER (
		USERID INT NOT NULL AUTO_INCREMENT,
		USERNAME VARCHAR(255),
		USERPASSWORD VARCHAR(255),
		USERLEVEL INT,
		PRIMARY KEY (USERID)
	)
*/
?>