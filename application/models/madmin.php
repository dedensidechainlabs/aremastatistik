<?php
class Madmin extends CI_Model{
	
	function gettallnewsletter(){
		$q = $this->db->query("SELECT * FROM TNEWSLETTER 
								ORDER BY SUBMITDATE DESC");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}
	
	function gettallmoreinfo(){
		$q = $this->db->query("SELECT * FROM TINFO 
								ORDER BY SUBMITDATE DESC");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}
	
	function gettallfreetrial(){
		$q = $this->db->query("SELECT * FROM TTRIAL 
								ORDER BY SUBMITDATE DESC");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}
	
	function gettallrequestpresentation(){
		$q = $this->db->query("SELECT * FROM TPRESENTATION 
								ORDER BY SUBMITDATE DESC");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}

	function gettallrequestquote(){
		$q = $this->db->query("SELECT * FROM TQUOTE 
								ORDER BY SUBMITDATE DESC");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}
	
	function gettallaffiliate(){
		$q = $this->db->query("SELECT * FROM TAFFILIATE 
								ORDER BY SUBMITDATE DESC");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}
	
	function gettalluser(){
		$q = $this->db->query("SELECT * FROM TUSER 
								ORDER BY SUBMITDATE DESC");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}
	
	function getallaffiliatedetail($uri3){
		$q = $this->db->query("SELECT * FROM TAFFILIATE,TUSER 
							   WHERE TAFFILIATE.USERID=TUSER.REFERENCEID
							   AND TUSER.REFERENCEID='$uri3'");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}
}
?>