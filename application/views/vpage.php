<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php 
    		foreach($pagetitle as $pagetitle): 
    		echo $pagetitle->CONTENTTITLE.' | '.$title; 
    		endforeach; 
    		?></title>
    <link rel="stylesheet" media="screen" href="<?php echo base_url();?>assets/css/sequencejs-theme.modern-slide-in.css" />
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/aremastats.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
  </head>
  <body>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	  <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand" href="<?php echo base_url();?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url();?>assets/img/logo-arema-stats.png" width="60px;"></a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <ul class="nav navbar-nav">
        	<li><a href="<?php echo base_url();?>"><h4>Arema Statistik</h4></a></li>	
		  </ul>
		  <ul class="nav navbar-nav navbar-right">
			<li><a href="<?php echo base_url();?>"><h4>Back to Home</h4></a></li>
			<li><a href="http://twitter.com/aremastats" target="_blank"><img src="<?php echo base_url();?>assets/img/twitter.png" width="40px"></a></li>
		  </ul>
		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<br><br>
	
	<?php $slug = $this->uri->segment(2);?>
	
	<div class="container">
		<?php foreach($content as $row):?>
		<div class="row">
			<br><br><br><br>
			<div class="col-md-10 col-md-offset-1">
				<?php if($row->CONTENTIMAGES!='' || $row->CONTENTIMAGES!=NULL){?>
				<div class="pageImages">
					<img src="<?php echo base_url();?>assets/img/<?php echo $row->CONTENTIMAGES;?>">
				</div>
				<?php } ?>
				<br>
				<div class="pageTitle">
					<h1><?php echo $row->CONTENTTITLE;?></h1>
					<br>
				</div>
				<div class="pageContent">
					<?php echo $row->CONTENTFULL;?>
				</div>
			</div>
		</div>
		<?php endforeach; ?>
		<br><br>
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-center">
				<?php $page = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
				<a href="https://twitter.com/aremastats" class="twitter-follow-button" data-show-count="false">Follow @aremastats</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo $page;?>" data-via="aremastats">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
				<a href="https://twitter.com/intent/tweet?screen_name=aremastats" class="twitter-mention-button" data-related="aremastats">Tweet to @aremastats</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
				
				<br><br>
				<a href="<?php echo base_url();?>"><button class="btn btn-warning">Back to Home</button></a>	
			</div>
		</div>
	</div>
	<br><br><br>
    <div class="container-fluid" id="footer">
		<div class="row">
			<div class="col-md-12 text-center">
				Copyright &copy; 2014. Arema Statistik. Powered by <a target="_blank" href="http://sidechainlabs.com/">Sidechain Labs</a>
			</div>
		</div>
	</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
	<script>
		if (typeof jQuery == 'undefined'){
	    	document.write(unescape('%3Cscript src="scripts/jquery-min.js" %3E%3C/script%3E'));
	 	}
	</script>
	<script src="<?php echo base_url();?>assets/js/jquery.sequence-min.js"></script>
	<script src="<?php echo base_url();?>assets/js/sequencejs-options.modern-slide-in.js"></script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-56078965-1', 'auto');
	  ga('send', 'pageview');

	</script>
	<script>
		$(function() {
		  $('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			  var target = $(this.hash);
			  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			  if (target.length) {
				$('html,body').animate({
				  scrollTop: target.offset().top
				}, 1000);
				return false;
			  }
			}
		  });
		});
	</script>
  </body>
</html>