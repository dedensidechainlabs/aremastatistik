<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" media="screen" href="<?php echo base_url();?>assets/css/sequencejs-theme.modern-slide-in.css" />
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/aremastats.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/set2.css" rel="stylesheet">
    
	<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
  </head>
  <body>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	  <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand" href="<?php echo base_url();?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url();?>assets/img/logo-arema-stats.png" width="60px;"></a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <ul class="nav navbar-nav">
        	<li><a href="<?php echo base_url();?>"><h4>Arema Statistik</h4></a></li>	
		  </ul>
		  <ul class="nav navbar-nav navbar-right">
			<li><a href="#focusplayer"><h4>Focus Player</h4></a></li>
			<li><a href="#news"><h4>News</h4></a></li>
			<li><a href="#matchprogramme"><h4>Match Programme</h4></a></li>
			<li><a href="#editorial"><h4>Editorial</h4></a></li>
			<li><a href="http://twitter.com/aremastats" target="_blank"><img src="<?php echo base_url();?>assets/img/twitter.png" width="40px" ></a></li>
		  </ul>
		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<div id="focusplayer"></div>
	<br><br><br><br>
	<div class="sequence-theme">
		<div id="sequence">

			<img class="sequence-prev" src="<?php echo base_url();?>assets/img/bt-prev.png" alt="Previous Frame" />
			<img class="sequence-next" src="<?php echo base_url();?>assets/img/bt-next.png" alt="Next Frame" />

			<ul class="sequence-canvas">
				<li class="animate-in">
					<h2 class="title">Move on untuk <br>Sengbah Kennedy</h2>
					<h3 class="subtitle">
						Ada yang berbeda dari penampilan singo edan musim ini, sosok berkuncir bernomor punggung 8 itu telah pergi ke Negeri Jiran yang konon melipat gandakan upahnya sampai 3x lipat. . .
						<br><br><a href="<?php echo base_url();?>page/move-on-untuk-sengbah-kennedy/"><button class="btn btn-warning">See More</button></a>
					</h3>
					<img class="model" src="<?php echo base_url();?>assets/img/sengbah-kennedy.png" alt="Juan Revi" />
				</li>
				<li>
					<h2 class="title">Best Tackle Arema ISL 2014<br>Juan Revi</h2>
					<h3 class="subtitle">
						Juan Revi adalah sosok penting dalam sistem pertahanan yang dibangun Arema di musim 2014, dia adalah pemain Arema dengan cirri khas Arek Malang yang sangat kental, keras dan lugas. Ini adalah musim ke tiga dia bersama Arema musim ini . . .
						<br><br><a href="<?php echo base_url();?>page/best-tackle-arema-isl-2014-juan-revi/"><button class="btn btn-warning">See More</button></a>
					</h3>
					<img class="model" src="<?php echo base_url();?>assets/img/juan-revi.png" alt="Juan Revi" />
				</li>
				<li>
					<h2 class="title">Top Score Arema ISL 2014<br>Samsul Arif</h2>
					<h3 class="subtitle">
						Pemain yang sepanjang karirnya hanya memperkuat Persibo Bojonegoro dan Persela Lamongan ini dikenal lewat kegemarannya melakukan tendangan salto ke gawang lawan. Torehan 13 gol dari 30 pertandingan musim lalu . . .
						<br><br><a href="<?php echo base_url();?>page/top-score-arema-isl-2014-samsul-arif/"><button class="btn btn-warning">See More</button></a>
					</h3>
					<img class="model" src="<?php echo base_url();?>assets/img/samsul-arif.png" alt="Samsul Arif" />
				</li>
				<li>
					<h2 class="title">Most Intercept Arema 2014<br>Purwaka Yudhi </h2>
					<h3 class="subtitle">
						Purwaka Yudhi, pemain Arema kelahiran Lampung ini merupakan defender tim Singo Edan di musim ISL 2014. Defender 30 tahun yang lalu ini bisa dibilang cukup konsisten dalam segi permainan. Dengan adanya sosok Purwaka Yudhi di lini belakang, Kurnia Meiga mampu dibuat tenang olehnya.
						<br><br><a href="<?php echo base_url();?>page/most-intercept-arema-2014-purwaka-yudhi/"><button class="btn btn-warning">See More</button></a>
					</h3>
					<img class="model" src="<?php echo base_url();?>assets/img/purwaka-yudhi.png" alt="Purwaka Yudhi" />
				</li>
				
				
			</ul>

			<!--<ul class="sequence-pagination">
				<li><img src="<?php echo base_url();?>assets/img/tn-samsul-arif.png" alt="Model 1" /></li>
				<li><img src="<?php echo base_url();?>assets/img/tn-juan-revi.png" alt="Model 2" /></li>
			</ul>-->

		</div>
	</div>
	
	<div id="news"></div>

	<div class="container-fluid"   style="background-color: rgba(0,136,211,0.9);">
		<br><br><br>
			<div class="row">
				<div class="col-lg-12">
					<br><div class="title-news text-center"><h1>NEWS</h1></div><br><br>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/2015-06-10 02.56.15 1.jpg" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Analisis Prematch : Mewaspadai Permainan Cepat Bali United</b></h2></div>
						<h4 style="line-height: 24px;">
							Minggu sore (14/6) Arema akan menantang Bali United dalam pertandingan bertajuk friendly match. Bermain di Stadion Kanjuruhan bakal jadi motivasi tersendiri bagi Bustomi cs...
							<br><br><a href="<?php echo base_url();?>page/analisis-prematch-mewaspadai-permainan-cepat-bali-united/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/bali-united-vs-arema-cronus-bali-island-cup-2015.jpg" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Arema v Bali United : Pelepas Rindu Arema</b></h2></div>
						<h4 style="line-height: 24px;">
							Pertandingan minggu sore (14/6) yang akan mempertemukan Arema Cronus v Bali United dapat dipastikan akan berlangsung menarik...
							<br><br><a href="<?php echo base_url();?>page/arema-v-bali-united-pelepas-rindu-arema/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/barito-actual-attacking-potrait.png" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Analisa Pertandingan Arema vs Barito (formasi&taktik)</b></h2></div>
						<h4 style="line-height: 24px;">
							"Bagi saya Barito tidak kalah malam ini, Barito pemenangnya". Kurang lebih seperti itulah statement yang dilontarkan Milo usai pertandingan. Sangat beralasan karena diatas lapangan juga bisa kita lihat, Arema yang menjadi tuan rumah sangat kesulitan dalam membongkar pertahanan Barito putera.
							<br><br><a href="<?php echo base_url();?>page/analisa-pertandingan-arema-vs-barito-formasi-dan-taktik/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/QNB-league.jpg" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Jadwal Arema Putaran 1 QNB League</b></h2></div>
						<h4 style="line-height: 24px;">
							
							<br><br><a href="<?php echo base_url();?>page/jadwal-arema-putaran-1-qnb-league/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/arif_1-1.jpg" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Match Analysis Post Match - Pergantian Formasi Merubah Keadaan</b></h2></div>
						<h4 style="line-height: 24px;">
							Arema mengawali pertandingan di Qatar National Bank League (QNB League) 2015 dengan hasil yang kurang memuaskan. Bermain di hadapan puluhan ribu Aremania yang hadir langsung di Stadion Kanjuruhan, Arema harus puas berbagi angka dengan Persija Jakarta dengan skor akhir 4-4. . .
							<br><br><a href="<?php echo base_url();?>page/match-analysis-post-match-pergantian-formasi-merubah-keadaan/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/timnas.jpg" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Performa Pemain Arema di Timnas Indonesia</b></h2></div>
						<h4 style="line-height: 24px;">
							Lima punggawa Singo Edan dipanggil memperkuat Timnas senior Indonesia untuk menjalani international friendly match menghadapi Kamerun dan Myanmar beberapa hari lalu. . .

							<br><br><a href="<?php echo base_url();?>page/performa-pemain-arema-di-timnas-indonesia/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/timnas.jpg" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Infografis Rapor Pemain Arema di Timnas Indonesia</b></h2></div>
						<h4 style="line-height: 24px;">
							
							<br><br><a href="<?php echo base_url();?>page/infografis-rapor-pemain-arema-di-timnas-indonesia/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/bali island cup jjadual.png" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Kunci Kemenangan Adalah Pergantian Pemain</b></h2></div>
						<h4 style="line-height: 24px;">
							Kemenangan 2-0 atas Persiram membuat Arema memperoleh 7 poin hasil dari dua kali kemenangan dan satu hasil seri yang membuat anak asuh Suharno dan Joko Susilo ini tak terkejar dari pesaing terdekat Pelita Bandung Raya dan Bali United Pusam dalam perebutan juara Bali Island Cup 2015. . .
							<br><br><a href="<?php echo base_url();?>page/kunci-kemenangan-adalah-pergantian-pemain/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/bali island cup jjadual.png" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Pre Match Analysis Arema v Persiram</b></h2></div>
						<h4 style="line-height: 24px;">
							Gelaran Bali Island Cup 2015 sampai pada pertandingan terakhir sore nanti. Dari 2 pertandingan yang sudah digelar Arema Cronus memimpin klasemen dengan 4 poin disusul Pelita Bandung Raya dengan 3 poin serta tuan rumah Bali United yang megoleksi 2 poin dan Persiram Raja Ampat di posisi buncit dengan 1 poin. . .
							<br><br><a href="<?php echo base_url();?>page/pre-match-analysis-arema-v-persiram/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/aremavsbali.png" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Post Match Bali United Pusam v Arema</b></h2></div>
						<h4 style="line-height: 24px;">
							Di pertandingan kedua turnamen Bali Island Cup, Arema gagal dapatkan poin penuh setelah ditahan imbang Bali United dengan skor 2-2. Arema gunakan formasi 4-3-3 dilaga ini dimana kiper malam hari itu dipercayakan kepada pemain asal Bali yaitu I Made Kadek Wardana. . .
							<br><br><a href="<?php echo base_url();?>page/post-match-bali-united-pusam-v-arema/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/bali island cup jjadual.png" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Pre Match Analysis Bali United v Arema</b></h2></div>
						<h4 style="line-height: 24px;">
							Setelah mengalahkan Pelita Bandung Raya dengan skor 2-0 melalui gol Feri Aman Saragih malam nanti Arema Cronus akan menghadapi tantangan Bali United Pusam dalam lanjutan Bali Island Cup 2015. . .
							<br><br><a href="<?php echo base_url();?>page/pre-match-analysis-bali-united-v-arema/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/IMG_20150312_222651.jpg" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Post Match Analysis Arema Cronus v Pelita Bandung Raya</b></h2></div>
						<h4 style="line-height: 24px;">
							Dalam laga perdana di ajang Bali Island Cup 2015, Arema sukses meraih poin penuh dari lawannya. Ini terbukti dari kemenangan Arema sore tadi yang berhasil menaklukkan Pelita Bandung Raya dengan skor 2-0 di Stadion Kapten Dipta Gianyar. . .
							<br><br><a href="<?php echo base_url();?>page/post-match-analysis-arema-cronus-v-pelita-bandung-raya/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/bali island cup jjadual.png" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Pre Match Analysis Arema v Pelita Bandung Raya</b></h2></div>
						<h4 style="line-height: 24px;">
							Turnamen bertajuk Bali Island Cup menjadi ajang pramusim selanjutnya yang akan dijajal skuad Arema Cronus. Di pertandingan pertama anak asuh Suharno dan Joko Susilo akan menghadapi hadangan dari Pelita Bandung Raya. . .
							<br><br><a href="<?php echo base_url();?>page/pre-match-analysis-arema-v-pelita-bandung-raya/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/o_19belud1cqohhurd76h34bdva.jpg" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Pre Match Analysis Arema v Persija</b></h2></div>
						<h4 style="line-height: 24px;">
							Ditundanya kick off Indonesia Super League tidak lantas membuat Arema Cronus berdiam diri. Serangkaian laga ujicoba sudah disusun untuk menyongsong kompetisi yang direncanakan akan dimulai 4 april mendatang. . .
							<br><br><a href="<?php echo base_url();?>page/pre-match-analysis-arema-v-persija/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/samsul_arif_arema_trofeo_persija_2015.jpg" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Kisah Antara Joko Susilo dan Samsul Arif</b></h2></div>
						<h4 style="line-height: 24px;">
							33 kilometer jarak yang membentang antara Cepu yang berada di Provinsi Jawa Tengah dan Bojonegoro yang berada di Provinsi Jawa Timur. . .
							<br><br><a href="<?php echo base_url();?>page/kisah-antara-joko-susilo-dan-samsul-arif/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/FAS.png" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Menyeruak di Tengah Padatnya Lini Tengah Arema</b></h2></div>
						<h4 style="line-height: 24px;">
							Perekrutan Ferry Aman Saragih sempat mengundang tanya bagi sebagian besar Aremania. Betapa tidak datangnya FAS sebutannya membuat lini tengah Arema semakin sesak setelah sebelumnya Singo Edan merekrut Sengbah Kennedy dan mempromosikan pemain Arema u21 Oky Derry. . .
							<br><br><a href="<?php echo base_url();?>page/menyeruak-di-tengah-padatnya-lini-tengah-arema/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/IMG_20150201_235643.jpg" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Post Match Analysis Final IIC 2014 - Arema v Persib</b></h2></div>
						<h4 style="line-height: 24px;">
							Arema berhasil mengawinkan tiga gelar di ajang pramusim kompetisi ISL 2015. Setelah sebelumnya menjuarai Trofeo Persija dan SCM Cup 2015, kali ini Singo Edan berhasil mengangkat trofi ketiga pada partai final Inter Island Cup 2014 setelah mengalahkan Persib Bandung lewat babak extra time dengan skor 2-1. . .
							<br><br><a href="<?php echo base_url();?>page/post-match-analysis-final-iic-2014-persib-v-arema/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/PhotoGrid_1422718573532.jpg" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Pre-Match Analysis Final IIC 2014 Persib v Arema</b></h2></div>
						<h4 style="line-height: 24px;">
							Pasca menjuarai turnamen pramusim SCM Cup, skuad Arema tidak bisa berleha-leha. Malam nanti di jakabaring Singo Edan akan menghadapi tantangan juara ISL 2014 Persib Bandung di laga final turnamen pramusim terpanjang di dunia Inter Island Cup 2014. . .
							<br><br><a href="<?php echo base_url();?>page/pre-match-analysis-final-iic-2014-persib-v-arema/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/IMG_20150128_081258.jpg" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Match Analysis Arema v Sriwijaya FC</b></h2></div>
						<h4 style="line-height: 24px;">
							Arema resmi menjadi juara dan berhak mengangkat trofi setelah di laga pamungkas SCM Cup 2015 dapat mengalahkan Sriwijaya FC. Bermain di Stadion Gelora Sriwijaya Jakabaring yang notabene kandang lawan, Singo Edan berhasil memetik kemenangan berkat gol heading Gilang Ginarsa di menit 75. . .
							<br><br><a href="<?php echo base_url();?>page/match-analysis-arema-v-sriwijaya-fc/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/gi.jpg" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<br><br><br><div class="title-news"><h2><b>Pre Match Analysis Arema v Sriwijaya</b></h2></div>
						<h4 style="line-height: 24px;">
							Setelah memetik kemenangan 1-0 atas Persebaya, Arema berhak lolos ke babak final turnamen pramusim SCM Cup. Di babak final Singo Edan sudah ditunggu salah satu tim terkuat di ISL musim depan Sriwijaya FC. . .
							<br><br><a href="<?php echo base_url();?>page/prematch-analysis-arema-v-sriwijaya/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/gi.jpg" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<br><br><br><div class="title-news"><h2><b>Match Analysis Arema v Persebaya</b></h2></div>
						<h4 style="line-height: 24px;">
							Derby klasik Jawa Timur kembali tersaji saat laga Arema melawan Persebaya di semifinal SCM Cup 2015, Minggu (25/1) yang dihelat di Stadion Gelora Sriwijaya Jakabaring, Palembang. . .
							<br><br><a href="<?php echo base_url();?>page/match-analysis-arema-v-persebaya/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/gi.jpg" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<br><br><br><div class="title-news"><h2><b>Prematch Analysis Arema v Persebaya</b></h2></div>
						<h4 style="line-height: 24px;">
							Berlaga di turnamen pramusim SCM Cup, Arema Cronus menunjukkan performa yang menjanjikan. Melenggang ke semifinal dengan status juara grup B setelah mengemas 9 poin hasil dari 3 kemenangan membawa Singo Edan menantang Persebaya finis di posisi 2 grup A. . .
							<br><br><a href="<?php echo base_url();?>page/prematch-analysis-arema-v-persebaya/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/PhotoGrid_1421975447748.jpg" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Match Analysis Arema vs Persipura</b></h2></div>
						<h4 style="line-height: 24px;">
							Arema mampu tampil edan pada pertandingan terakhir babak penyisihan SCM Cup 2015. Sore kemaren Singo Edan sukses menaklukkan Persipura Jayapura dengan skor telak 4-1. Dengan ini Arema menjadi capolista di klasemen grup B. . .
							<br><br><a href="<?php echo base_url();?>page/post-match-analysis-arema-vs-persipura/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/gi.jpg" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<br><br>
						<div class="title-news"><h2><b>Pre Match Analysis Arema vs Persipura</b></h2></div>
						<h4 style="line-height: 24px;">
							Berlaga di turnamen SCM Cup Arema berhasil meraih dua hasil positif, dua kemenangan yang diraih masing-masing 5-2 melawan Mitra Kukar dan 2-1 atas Persela membuat Singo Edan memimpin klasemen sementara grup B dengan 6 poin. . .
							<br><br><a href="<?php echo base_url();?>page/pre-match-analysis-arema-vs-persipura/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/match-analysis-arema-persela.png" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<br><br>
						<div class="title-news"><h2><b>Post Match Analysis Persela vs Arema</b></h2></div>
						<h4 style="line-height: 24px;">
							Arema kembali meraih kemenangan keduanya di ajang SCM Cup 2015 setelah sebelumnya menaklukkan Mitra Kukar dengan skor 5-2. Kemenangan terbaru didapat kala Singo Edan bertemu dengan Persela Lamongan yang berakhir dengan skor 2-1. . . .
							<br><br><a href="<?php echo base_url();?>page/post-match-analysis-persela-arema/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/gi.jpg" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<br><br>
						<div class="title-news"><h2><b>Pre Match Analysis Arema vs Persela</b></h2></div>
						<h4 style="line-height: 24px;">
							Start bagus Arema di turnamen SCM Cup diawali dengan menumbangkan Mitra Kukar dengan skor 5-2. Di laga kedua Persela Lamongan yang menaklukan Persipura 1-0 sudah menanti untuk menguji kekuatan Singo Edan. . .
							<br><br><a href="<?php echo base_url();?>page/pre-match-analysis-arema-vs-persela/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/match-analysis-arema-mitra-kukar.jpg" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<br><br>
						<div class="title-news"><h2><b>Match Analysis Arema vs Mitra Kukar</b></h2></div>
						<h4 style="line-height: 24px;">
							Dalam laga perdana di ajang Surya Citra Media Cup 2015, Arema sukses meraih poin penuh dari lawannya. Ini terbukti dari kemenangan telak yang Arema raih atas Mitra Kukar dengan skor 5-2 di Stadion Kanjuruhan, kemarin sore. . . 
							<br><br><a href="<?php echo base_url();?>page/match-analysis-arema-vs-mitra-kukar/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/gi.jpg" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<br><br>
						<div class="title-news"><h2><b>Match Preview Arema vs Mitra Kukar</b></h2></div>
						<h4 style="line-height: 24px;">
							Pasca berpartisipasi di Trofeo Persija, Arema sudah ditunggu ajang pramusim selanjutnya. Di turnamen bertajuk  SCM Cup, tuan rumah Arema akan bertemu Mitra Kukar, Persela Lamongan serta Persipura Jayapura. . .
							<br><br><a href="<?php echo base_url();?>page/match-preview-arema-vs-mitra-kukar/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/harga-tiket-trofeo-persija-turun-ILK9LVSjRb.png" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Analisa Permainan Arema di Trofeo Persija</b></h2></div>
						<h4 style="line-height: 24px;">
							Arema telah melakoni turnamen pra-musim perdananya di musim kompetisi 2015. Turun di ajang Trofeo Persija, skuad Singo Edan menorehkan 1 kemenangan dan 1 kekalahan. Kemenangan didapat saat mengalahkan Sriwijaya FC 4-2 via adu pinalti. . .
							<br><br><a href="<?php echo base_url();?>page/analisa-permainan-arema-di-trofeo-persija/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/harga-tiket-trofeo-persija-turun-ILK9LVSjRb.png" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Membedah Kekuatan Tim di Trofeo Persija</b></h2></div>
						<h4 style="line-height: 24px;">
							Gelaran Trofeo Persija 2015 kali ini digelar pada 11 Januari 2015 di Stadion Gelora Bung Karno, Jakarta.  Tahun ini merupakan keempat kalinya ajang ini digelar yang sebelumnya melahirkan juara dua kali yaitu Persija Jakarta (2011 & 2012) dan Arema (2013). . .
							<br><br><a href="<?php echo base_url();?>page/membedah-kekuatan-tim-di-trofeo-persija/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><br>
					<img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/yao.png" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Catatan Impresif Yao Ruddy</b></h2></div>
						<h4 style="line-height: 24px;">
							Musim kompetisi 2015 Arema Cronus punya pemain asing anyar berposisi penyerang, dia berkebangsaan Liberia dan mempunyai predikat sebagai Top Score Divisi Utama musim lalu dengan 17 gol, dia adalah Yao Rudy. . .
							<br><br><a href="<?php echo base_url();?>page/catatan-impresif-yao-ruddy/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><br>
					<img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/joksusr.jpg" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Reuni Antar Murid dan Guru</b></h2></div>
						<h4 style="line-height: 24px;">
							Gelaran Trofeo Persija tahun ini akan mempertemukan Persija Jakarta, Sriwijaya FC dan Arema Cronus. Duel 3 tim dengan nama besar di Indonesia ini memiliki makna spesial bagi Joko Susilo. . .
							<br><br><a href="<?php echo base_url();?>page/reuni-antar-murid-dan-guru/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/arema-preseason-vs-uitm.jpg" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Arema Jajal Formasi 4-1-3-2</b></h2></div>
						<h4 style="line-height: 24px;">
							Stadion Kanjuruhan sore 26 Desember 2014 atau biasa dikenal di Inggris dengan istilah "Boxing Day" menggelar pertandingan uji coba antara Arema Cronus v UiTM dari Malaysia. Pertandingan yang cukup mengundang banyak Aremania ini berakhir dengan kemenangan Arema yang ketiga beruntun...
							<br><br><a href="<?php echo base_url();?>page/arema-jajal-formasi-4-1-3-2/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/news-fabiano.jpg" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Meretas Asa Juara dari Fabiano</b></h2></div>
						<h4 style="line-height: 24px;">
							Rencana Arema Crounus yang ingin mendatangkan pemain belakang berkarakter seperti Pierre Njanka Beyaka akhirnya terealisasi, dia adalah Fabiano Da Rosa Beltrame eks kapten Persija yang beberapa hari lalu mengunggah fotonya yang sedang menandatangani kontrak serta menggunakan jersey Arema... 
							<br><br><a href="<?php echo base_url();?>page/meretas-asa-juara-dari-fabiano/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>
				</div>
			</div>
			
		<br><br><br>
	</div>
	
	<div id="matchprogramme"></div>
	<br><br><br><br>
	<div class="container">
		<div class="row">
			<br><br><br>
			<div class="col-lg-4 col-lg-offset-2 text-center">
				<img id="object" class="slideRight" width="90%" src="<?php echo base_url();?>assets/img/match-programme2-2.png" alt="Arema Statistik">
			</div>
			<div class="col-lg-4">
				<div id="object" class="slideLeft" style="color:#fff;">
				<br>
				<h1>Download Match Programme Vol 2 #2</h1>
				<h4 style="line-height:26px;">
					Malam ini Arema Cronus harus kembali berjibaku dilanjutan laga QNB League dengan menjamu Bariot Putera.
				</h4>
				<form method="post" action="<?php echo base_url();?>download" target="_blank">
					<input type="hidden" value="arema-statistik-match-programme2-2.pdf" name="txtlink">
					<input type="submit" class="btn btn-warning" value="DOWNLOAD">
				</form>
				<br><br><br><br><br>
				</div>
			</div>
		</div>
		<br><br>
		<div class="row">
			<div class="col-lg-3">
				<div class="grid">
					<figure class="effect-apollo">
						<img src="<?php echo base_url();?>assets/img/match-programme2-1.png" width="100%" height="100%">
						<figcaption>
							<p class="text-center"><br><br>Match Programme Vol 2 #1<br><br>
							
							</p>
						</figcaption>			
					</figure>
					<form method="post" action="<?php echo base_url();?>download" target="_blank">
						<input type="hidden" value="arema-statistik-match-programme2-1.pdf" name="txtlink">
						<input type="submit" class="btn btn-warning" value="DOWNLOAD">
					</form>
				</div>
			</div>
			
			<div class="col-lg-3">
				<div class="grid">
					<figure class="effect-apollo">
						<img src="<?php echo base_url();?>assets/img/match-programme-12.png" width="100%" height="100%">
						<figcaption>
							<p class="text-center"><br><br>Match Programme #12<br><br>
							
							</p>
						</figcaption>			
					</figure>
					<form method="post" action="<?php echo base_url();?>download" target="_blank">
						<input type="hidden" value="arema-statistik-match-programme-11.pdf" name="txtlink">
						<input type="submit" class="btn btn-warning" value="DOWNLOAD">
					</form>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="grid">
					<figure class="effect-apollo">
						<img src="<?php echo base_url();?>assets/img/match-programme-11.png" width="100%" height="100%">
						<figcaption>
							<p class="text-center"><br><br>Match Programme #11<br><br>
							
							</p>
						</figcaption>			
					</figure>
					<form method="post" action="<?php echo base_url();?>download" target="_blank">
						<input type="hidden" value="arema-statistik-match-programme-11.pdf" name="txtlink">
						<input type="submit" class="btn btn-warning" value="DOWNLOAD">
					</form>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="grid">
					<figure class="effect-apollo">
						<img src="<?php echo base_url();?>assets/img/match-programme-10.png" width="100%" height="100%">
						<figcaption>
							<p class="text-center"><br><br>Match Programme #10<br><br>
							
							</p>
						</figcaption>			
					</figure>
					<form method="post" action="<?php echo base_url();?>download" target="_blank">
						<input type="hidden" value="arema-statistik-match-programme-10.pdf" name="txtlink">
						<input type="submit" class="btn btn-warning" value="DOWNLOAD">
					</form>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="grid">
					<figure class="effect-apollo">
						<img src="<?php echo base_url();?>assets/img/match-programme-9.png" width="100%" height="100%">
						<figcaption>
							<p class="text-center"><br><br>Match Programme #9<br><br>
							
							</p>
						</figcaption>			
					</figure>
					<form method="post" action="<?php echo base_url();?>download" target="_blank">
						<input type="hidden" value="arema-statistik-match-programme-9.pdf" name="txtlink">
						<input type="submit" class="btn btn-warning" value="DOWNLOAD">
					</form>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="grid">
					<figure class="effect-apollo">
						<img src="<?php echo base_url();?>assets/img/match-programme-8.png" width="100%" height="100%">
						<figcaption>
							<p class="text-center"><br><br>Match Programme #8<br><br>
							
							</p>
						</figcaption>			
					</figure>
					<form method="post" action="<?php echo base_url();?>download" target="_blank">
						<input type="hidden" value="arema-statistik-match-programme-8.pdf" name="txtlink">
						<input type="submit" class="btn btn-warning" value="DOWNLOAD">
					</form>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="grid">
					<figure class="effect-apollo">
						<img src="<?php echo base_url();?>assets/img/match-programme-7.png" width="100%" height="100%">
						<figcaption>
							<p class="text-center"><br><br>Match Programme #7<br><br>
							
							</p>
						</figcaption>			
					</figure>
					<form method="post" action="<?php echo base_url();?>download" target="_blank">
						<input type="hidden" value="arema-statistik-match-programme-7.pdf" name="txtlink">
						<input type="submit" class="btn btn-warning" value="DOWNLOAD">
					</form>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="grid">
					<figure class="effect-apollo">
						<img src="<?php echo base_url();?>assets/img/match-programme-6.png" width="100%" height="100%">
						<figcaption>
							<p class="text-center"><br><br>Match Programme #6<br><br>
							
							</p>
						</figcaption>			
					</figure>
					<form method="post" action="<?php echo base_url();?>download" target="_blank">
						<input type="hidden" value="arema-statistik-match-programme-6.pdf" name="txtlink">
						<input type="submit" class="btn btn-warning" value="DOWNLOAD">
					</form>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="grid">
					<figure class="effect-apollo">
						<img src="<?php echo base_url();?>assets/img/match-programme-5.png" width="100%" height="100%">
						<figcaption>
							<p class="text-center"><br><br>Match Programme #5<br><br>
							
							</p>
						</figcaption>			
					</figure>
					<form method="post" action="<?php echo base_url();?>download" target="_blank">
						<input type="hidden" value="arema-statistik-match-programme-5.pdf" name="txtlink">
						<input type="submit" class="btn btn-warning" value="DOWNLOAD">
					</form>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="grid">
					<figure class="effect-apollo">
						<img src="<?php echo base_url();?>assets/img/match-programme-4.png" width="100%" height="100%">
						<figcaption>
							<p class="text-center"><br><br>Match Programme #4<br><br>
							
							</p>
						</figcaption>			
					</figure>
					<form method="post" action="<?php echo base_url();?>download" target="_blank">
						<input type="hidden" value="arema-statistik-match-programme-4.pdf" name="txtlink">
						<input type="submit" class="btn btn-warning" value="DOWNLOAD">
					</form>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="grid">
					<figure class="effect-apollo">
						<img src="<?php echo base_url();?>assets/img/match-programme-3.png" width="100%" height="100%">
						<figcaption>
							<p class="text-center"><br><br>Match Programme #3<br><br>
							
							</p>
						</figcaption>			
					</figure>
					<form method="post" action="<?php echo base_url();?>download" target="_blank">
						<input type="hidden" value="arema-statistik-match-programme-3.pdf" name="txtlink">
						<input type="submit" class="btn btn-warning" value="DOWNLOAD">
					</form>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="grid">
					<figure class="effect-apollo">
						<img src="<?php echo base_url();?>assets/img/match-programme-2.png" width="100%" height="100%">
						<figcaption>
							<p class="text-center"><br><br>Match Programme #2<br><br>
							
							</p>
						</figcaption>			
					</figure>
					<form method="post" action="<?php echo base_url();?>download" target="_blank">
						<input type="hidden" value="arema-statistik-match-programme-2.pdf" name="txtlink">
						<input type="submit" class="btn btn-warning" value="DOWNLOAD">
					</form>
				</div>
			</div>
		</div>
	</div>
	
	
	<div id="editorial"></div>

	<div class="container-fluid"   style="background-color: rgba(0,136,211,0.9);">
		<br><br><br>
			<div class="row">
				<div class="col-lg-12">
					<br><div class="title-news text-center"><h1>EDITORIAL</h1></div><br><br>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/PT Liga Indonesia-1200x630.jpg" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>PR Baru Para Juru Taktik</b></h2></div>
						<h4 style="line-height: 24px;">
							Tertundanya kick off Liga Super Indonesia memang bukan barang baru. Setiap musim PT Liga Indonesia selaku operator kompetisi selalu gagal memberikan kepastian jadwal bagi kontestan Indonesia Super League ataupun Divisi Utama. Di musim ini  PT Liga Indonesia belum sembuh dari penyakit tersebut. . .
							<br><br><a href="<?php echo base_url();?>page/pr-baru-para-juru-taktik/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-lg-offset-2">
					<br><img id="object" class="slideRight" width="100%" src="<?php echo base_url();?>assets/img/logo-arema-stats.png" alt="Arema Statistik">
				</div>
				<div class="col-lg-5">
					<div class="news-description">
						<div class="title-news"><h2><b>Keindahan Sepak Bola Dalam Imajinasi yang Berbeda</b></h2></div>
						<h4 style="line-height: 24px;">
							Minggu malam, bertempat di Stadion Jakabaring Palembang euforia itu akan gelar kedua turnamen preseason yang di ikuti Arema jelang bergulirnya ISL musim 2015 tumpah. . .
							<br><br><a href="<?php echo base_url();?>page/keindahan-sepak-bola-dalam-imajinasi-yang-berbeda/"><button class="btn btn-warning">See More</button></a><br><br>
						</h4>
					</div>	
				</div>
			</div>
			<br>
		<br><br><br>	
	</div>
	
	
	
	<div class="container-fluid" id="footer">
		<div class="row">
			<div class="col-md-12 text-center">
				Copyright &copy; 2014. Arema Statistik. Powered by <a target="_blank" href="http://sidechainlabs.com/">Sidechain Labs</a>
			</div>
		</div>
	</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
	<script>
		if (typeof jQuery == 'undefined'){
	    	document.write(unescape('%3Cscript src="scripts/jquery-min.js" %3E%3C/script%3E'));
	 	}
	</script>
	<script src="<?php echo base_url();?>assets/js/jquery.sequence-min.js"></script>
	<script src="<?php echo base_url();?>assets/js/sequencejs-options.modern-slide-in.js"></script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-56078965-1', 'auto');
	  ga('send', 'pageview');

	</script>
	<script>
		$(function() {
		  $('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			  var target = $(this.hash);
			  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			  if (target.length) {
				$('html,body').animate({
				  scrollTop: target.offset().top
				}, 1000);
				return false;
			  }
			}
		  });
		});
	</script>
  </body>
</html>