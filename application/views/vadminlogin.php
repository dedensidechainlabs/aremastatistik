<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>

    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/aremastats.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
  </head>
  <body>
    
  	<div class="container">
  		<div class="row">
  			<div class="col-md-4 col-md-offset-4 text-center">
  				<br><br><br><br>
				<div class="boxadmin">
  				<form method="post" action="<?php echo base_url();?>astatadmin/login">
  					<div class="logoadmin">
  						<img src="<?php echo base_url();?>assets/img/logo-arema-stats.png">
  					</div>
  					<h3>ADMINISTRATOR PAGE</h3>
  					<br>
  					<div class="form-group">
					    <label for="txtusername">USERNAME</label>
					    <input type="text" class="form-control" name="txtusername" id="txtusername" placeholder="Enter Username">
				  	</div>
  					<div class="form-group">
					    <label for="txtpassword">PASSWORD</label>
					    <input type="password" class="form-control" name="txtpassword" id="txtpassword" placeholder="Enter Password">
				  	</div>
  					<input type="submit" name="txtlogin" value="LOGIN" class="btn btn-warning">
  					
  					<?php echo (isset($error)?$error:"")?>
  				</form>
  				</div>
  			</div>
  		</div>
  	</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-56078965-1', 'auto');
	  ga('send', 'pageview');

	</script>
  </body>
</html>