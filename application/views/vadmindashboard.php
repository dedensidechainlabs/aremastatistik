<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>

    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/aremastats.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
  </head>
  <body>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	  <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand" href="http://aremastatistik.com/">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url();?>assets/img/logo-arema-stats.png" width="60px;"></a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			
		  <ul class="nav navbar-nav navbar-right">
			<li><a href="<?php echo base_url();?>astatadmin/logout"><h5><br>LOGOUT</h5></a></li>
		  </ul>
		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<br><br>
				<br><br>
				<br><br>
				<h3>Hi <?php echo (isset($username)?$username:"")?>! Welcome to Administrator Page!</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				
				<br><br>
				
				<table class="table table-hover table-bordered">
					<tr id="tabletitle">
						<td colspan="3"><h4>TRACKING CLICK MATCH PROGRAMME</h4></td>
					</tr>
					<tr>
						<th>#</th>
						<th>FILE</th>
						<th>CLICK</th>
					</tr>
					<?php $counter=1; foreach($qprogramme as $qprogramme): ?>
					<tr>
						<td><?php echo $counter;?>.</td>
						<td><?php echo $qprogramme->FILE;?></td>
						<td><?php echo $qprogramme->CLICK;?></td>
					</tr>
					<?php $counter++; endforeach; ?>
				</table>
			</div>
		</div>
	</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-56078965-1', 'auto');
	  ga('send', 'pageview');

	</script>
  </body>
</html>