<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>

    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/aremastats.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
  </head>
  <body>
    <nav class="navbar navbar-default" role="navigation">
	  <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand" href="http://aremastatistik.com/">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url();?>assets/img/logo-arema-stats.png" width="60px;"></a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			
		  <ul class="nav navbar-nav navbar-right">
			<li><a href="http://aremastatistik.com/"><h3>arema statistik</h3></a></li>
			<li><a href="http://twitter.com/aremastats" target="_blank"><img src="<?php echo base_url();?>assets/img/twitter.png" width="40px" style="margin-top: 10px;"></a></li>
		  </ul>
		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<div class="container">
		<div class="row">
			<br>
			<br>
			<br>
			<br>
			<div class="col-lg-4 col-lg-offset-2 text-center">
				<img width="80%" src="<?php echo base_url();?>assets/img/match-programme-11.png" alt="Arema Statistik">
			</div>
			<div class="col-lg-4">
				<div style="color:#fff;">
				<br>
				<h1>Download Match Programme #11</h1>
				<h4 style="line-height:26px;">
					Minggu, 25 Oktober 2014<br>
					Menjadi laga hidup mati untuk Arema. Hasil pertandingan bakal menentukan apakah Arema bisa menginjakan satu kaki ke babak semifinal atau bahkan harus terhenti sejenak menunggu laga terakhir saat away ke Semen Padang. Now or Never!
				</h4>
				<form method="post" action="<?php echo base_url();?>download">
					<input type="submit" class="btn btn-warning" value="DOWNLOAD">
				</form>
				</div>
			</div>
		
		</div>
	</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
  </body>
</html>