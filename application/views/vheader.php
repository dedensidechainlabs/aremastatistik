<!doctype html>
<!--[if lt IE 8 ]><html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta name="author" content="">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title; ?></title>

<!-- main JS libs -->
<script src="<?php echo base_url().'assets/';?>js/libs/jquery-1.11.0.min.js"></script>
<script src="<?php echo base_url().'assets/';?>js/libs/jquery-ui.min.js"></script>
<script src="<?php echo base_url().'assets/';?>js/libs/bootstrap.min.js"></script>

<!-- Style CSS -->
<link href="<?php echo base_url().'assets/';?>css/bootstrap.css" media="screen" rel="stylesheet">
<link href="<?php echo base_url().'assets/';?>css/style.css" media="screen" rel="stylesheet">

<!-- General Scripts -->
<script src="<?php echo base_url().'assets/';?>js/general.js"></script>

<!-- CarouFredSel  -->
<script src="<?php echo base_url().'assets/';?>js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script src="<?php echo base_url().'assets/';?>js/jquery.touchSwipe.min.js"></script>

<!-- Lightbox prettyPhoto -->
<link href="<?php echo base_url().'assets/';?>css/prettyPhoto.css" rel="stylesheet">
<script src="<?php echo base_url().'assets/';?>js/jquery.prettyPhoto.js"></script>

<!-- Responsive Menu -->
<link rel="stylesheet" href="<?php echo base_url().'assets/';?>css/slicknav.css">
<script src="<?php echo base_url().'assets/';?>js/jquery.slicknav.min.js"></script>
<script>
    $(document).ready(function(){
        $('.menu').slicknav();
    });
</script>

<!-- Scroll Bars -->
<script src="<?php echo base_url().'assets/';?>js/jquery.mousewheel.js"></script>
<script src="<?php echo base_url().'assets/';?>js/jquery.jscrollpane.min.js"></script>
<script>
    jQuery(function() {
        jQuery('.scrollbar').jScrollPane({
            autoReinitialise: true,
            verticalGutter: 0
        });
    });
</script>

<!-- Video Player -->
<link href="<?php echo base_url().'assets/';?>css/video-js.css" rel="stylesheet">
<script src="<?php echo base_url().'assets/';?>js/video.js"></script>
<script>
    videojs.options.flash.swf = "js/video-js.swf";
</script>

<!-- Audio Player -->
<link href="<?php echo base_url().'assets/';?>css/jplayer.css" rel="stylesheet">
<script src="<?php echo base_url().'assets/';?>js/jplayer/jquery.jplayer.min.js"></script>
<script src="<?php echo base_url().'assets/';?>js/jplayer/jplayer.playlist.min.js"></script>
<script src="<?php echo base_url().'assets/';?>js/jplayer/jquery.transform2d.js"></script>
<script src="<?php echo base_url().'assets/';?>js/jplayer/jquery.grab.js"></script>
<script src="<?php echo base_url().'assets/';?>js/jplayer/mod.csstransforms.min.js"></script>
<script src="<?php echo base_url().'assets/';?>js/jplayer/circle.player.js"></script>

<!-- Graph Builder -->
<script src="https://www.google.com/jsapi"></script>

<!--[if lt IE 9]>
<script src="js/libs/html5shiv.js"></script>
<script src="js/libs/respond.min.js"></script>
<![endif]-->
</head>

<body>
<div class="body-wrap">