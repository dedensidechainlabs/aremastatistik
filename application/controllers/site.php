<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        $this->load->model('mtracking');
        $this->load->model('mcontent');
    }
	
	public function index()
	{
		$data['title'] = 'Arema Statistik';
		$this->load->view('vindex',$data);
	}	
	
	function download()
	{
		$txtlink = $this->input->post("txtlink");
		$uri1 = $this->uri->segment(1);
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		
		$this->load->helper('date');
		$ipaddress = $this->input->ip_address();
		$datenow = date("Y-m-d");
		$timenow = date('H:i:s');
		
		$data['ipaddress'] = $ipaddress;
		$data['datenow'] = $datenow;
		$data['timenow'] = $timenow;
	
		$tracking = array('TRACKINGFILE' => $txtlink,
						'TRACKINGIP' => $ipaddress,
						'TRACKINGDATE' => $datenow,
						'TRACKINGTIME' => $timenow
						);
		$this->mtracking->save_tracking($tracking);
		redirect("file/$txtlink","refresh");
	}
	
	function page($slug='')
	{
		$data['title'] = 'Arema Statistik';
		$uri1 = $this->uri->segment(1);
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		$slug = $uri2;
		$data["pagetitle"] = $this->mcontent->getContent($slug);
		$data["content"] = $this->mcontent->getContent($slug);
		$this->load->view('vpage',$data);	
	}
}