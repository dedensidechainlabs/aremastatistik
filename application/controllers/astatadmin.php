<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Astatadmin extends CI_Controller {
	
	function __construct()
    {
        parent::__construct();
        $this->load->model('mtracking');
    }
	
	function index()
	{
		$data['title'] = 'Arema Statistik';
		$this->load->view('vadminlogin',$data);
	}	

	function login()
	{
		$txtusername = $this->input->post("txtusername");
		$txtpassword = md5($this->input->post("txtpassword"));
		$qcekusername = $this->db->query("SELECT * FROM TUSER WHERE USERNAME='$txtusername' AND USERPASSWORD='$txtpassword'");
		$valcekusername = $qcekusername->num_rows();
		if($valcekusername == 1){
			$row = $qcekusername->row();
			$userlevel = $row->USERLEVEL;
			$this->load->library('session');
			$this->session->set_userdata('username', $txtusername);
			$this->session->set_userdata('userlevel', $userlevel);
			redirect('astatadmin/dashboard','refresh');
		}else{
			$data['title'] = "Arema Statistik";
			$data['error'] = "<h5 style='color:#ff0000;'>Invalid Username or Password!<br>Please Try Again!</h5>";
			$this->load->view('vadminlogin',$data);
		}
	}

	function dashboard()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Arema Statistik";
			$data['username'] = $this->session->userdata('username');
			$data['qprogramme'] = $this->mtracking->gettracking();
			$this->load->view('vadmindashboard',$data);
		}else{
			redirect('astatadmin','refresh');
		}
	}
	
	function logout(){
		$this->session->sess_destroy();
		redirect('astatadmin','refresh');
	}
}